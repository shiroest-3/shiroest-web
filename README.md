OPEN COMMAND PROMPT
--------------------------------

clone repository  ->  git clone https://shiroest@bitbucket.org/shiroest-3/shiroest-web.git

cd shiroest-web

cd shiroest-www

update ->  npm update

Create `dist` folder -> ng build

To run -> ng serve --open


INSTALL ALL DEPENDENCIES FROM BELOW
---------------------------------

Angular    - 8.2.13

Angular CLI - 8.3.17

Node.JS    - 10.16.2 

MnongaDB   - 4.x


IF YOU FIND AN ISSUE WIHT ANGULAR CLI
-------------------------------------
npm install -g @angular/cli@latest
