import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AddressComponent>
  ) { }

  ngOnInit() {
  }

  // const dialogRef = this.dialog.open(AddressComponent, {panelClass: 'custom-dialog-container'});

  close() {
    this.dialogRef.close();
  }
}
