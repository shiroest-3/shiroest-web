import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { Pants } from '../../product';

@Component({
  selector: 'app-pants',
  templateUrl: './pants.component.html',
  styleUrls: ['./pants.component.scss']
})
export class PantsComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  pants: Pants[] = [];
  urlName: string;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getPants().subscribe((pants: Pants[]) => {
      this.pants = pants;
      console.log(this.productService);
    });

    this.urlName = 'pant'
  }
}
