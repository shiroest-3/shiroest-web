import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { Shirts } from '../../product';

@Component({
  selector: 'app-shirts',
  templateUrl: './shirts.component.html',
  styleUrls: ['./shirts.component.scss']
})
export class ShirtsComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  shirt: Shirts[] = [];
  urlName: string;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getShirts().subscribe((shirt: Shirts[]) => {
      this.shirt = shirt;
      console.log(this.productService);
    });

    this.urlName = 'shirt';
  }

}
