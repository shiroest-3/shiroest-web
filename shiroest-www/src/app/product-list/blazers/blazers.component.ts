import { Component, OnInit, Input, APP_ID } from '@angular/core';
import { ProductService } from '../../product.service';
import { Blazers } from '../../product';

@Component({
  selector: 'app-blazers',
  templateUrl: './blazers.component.html',
  styleUrls: ['./blazers.component.scss']
})
export class BlazersComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  products: Blazers[] = [];
  urlName: string;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getProducts().subscribe((products: Blazers[]) => {
      this.products = products;
      console.log(this.productService);
    });

    this.urlName = 'blazer';
  }
}
