import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { Blazers, Shirts, Pants } from '../../product';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  hide: boolean = true;
  pantSize: boolean = false;
  otherSize: boolean = false;
  blazer: boolean = false;
  shirt: boolean = false;
  dynamicImage: any;
  dynamicImage1: any;
  index: number;
  url: string;
  product: Blazers = new Blazers( -1, '', 'name' , 0, 0, '', '');
  shirts: Shirts = new Shirts(-1, '', 'name' , 0, 0, '', '');
  pants: Pants = new Pants(-1, '', 'name' , 0, 0, '', '');


  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => this.index = params['id']);
    if (this.index <= 10 ) {
    this.productService.getProduct(this.index).subscribe((product: Blazers) => {
      this.product = product;
      this.otherSize = true;
      this.blazer = true;
    });
  }

    this.route.params.subscribe(params => this.index = params['id']);
    if (this.index >= 11) {
      this.productService.getShirt(this.index).subscribe((shirts: Shirts) => {
        this.shirts = shirts;
        this.otherSize = true;
        this.shirt = true;
      });
    }

    this.route.params.subscribe(params => this.index = params['id']);
    if (this.index >= 31) {
      this.productService.getPant(this.index).subscribe((pants: Pants) => {
        this.pants = pants;
        this.pantSize = true;
      });
    }
  }

  getImage(event) {
    this.hide = false;
    this.dynamicImage = event.target.src;
  }
}
