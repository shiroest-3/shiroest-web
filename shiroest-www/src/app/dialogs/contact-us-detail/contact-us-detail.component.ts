import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-contact-us-detail',
  templateUrl: './contact-us-detail.component.html',
  styleUrls: ['./contact-us-detail.component.scss']
})
export class ContactUsDetailComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ContactUsDetailComponent>
  ) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }
}
