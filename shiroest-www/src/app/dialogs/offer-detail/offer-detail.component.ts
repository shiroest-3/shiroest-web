import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-offer-detail',
  templateUrl: './offer-detail.component.html',
  styleUrls: ['./offer-detail.component.scss']
})
export class OfferDetailComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  constructor(private dialog: MatDialog,
              private dialogdRef: MatDialogRef<OfferDetailComponent>) { }

  ngOnInit() {
  }

  close() {
    this.dialogdRef.close(OfferDetailComponent);
  }
}
