import { Component, OnInit, HostListener } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { AddressComponent } from '../address/address.component';
import { HowItWorkComponent } from '../how-it-work/how-it-work.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  navbarCollapsed = true;
  showHamburger = true;
  hideHamburger = false;
  toggleNavShadow = false;
  navlink: any;
  ee: any;

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit() {
  }

@HostListener('window:scroll', ['$event']) getScrollHeight(event) {
   if (window.pageYOffset === 0) {
     this.toggleNavShadow = false;
   } else {
     this.toggleNavShadow = true;
   }
}

  address() {
    this.dialog.open(AddressComponent, {panelClass: 'custom-dialog-container'});
  }

  howitWork() {
    this.dialog.open(HowItWorkComponent);
  }

    toggleNavbarCollapsing() {
      this.navbarCollapsed = !this.navbarCollapsed;
      this.showHamburger = !this.showHamburger;
      this.hideHamburger = !this.hideHamburger;
  }
}
