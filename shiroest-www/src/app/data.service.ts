import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const blazers = [
      { id: 1, name: 'Black Tuxedo 2 piece suite',  mrp: 5980, discount: 90, image: 'assets/blazers/one-one.jpg',
      imgone: 'assets/blazers/one-two.jpg', imgtwo: 'assets/blazers/one-three.jpg'},
      {id: 2, name: 'Brown checked Blazer',  mrp: 9980, discount: 97, image: 'assets/blazers/three-one.jpeg',
      imgone: 'assets/blazers/three-two.jpeg', imgtwo: 'assets/blazers/three-three.jpeg'},
      {id: 3, name: 'Black tuxedo 2 piece',  mrp: 3980, discount: 77, image: 'assets/blazers/two-one.jpg',
      imgone: 'assets/blazers/two-two.jpg', imgtwo: 'assets/blazers/two-three.jpg'},
      {id: 4, name: 'Grey checked suite 2 piece',  mrp: 4980, discount: 90, image: 'assets/blazers/four-one.jpg',
      imgone: 'assets/blazers/four-two.jpg', imgtwo: 'assets/blazers/four-three.jpg'},
      {id: 5, name: 'Printed suite',  mrp: 10980, discount: 92, image: 'assets/blazers/five-one.jpeg',
      imgone: 'assets/blazers/five-two.jpeg', imgtwo: 'assets/blazers/five-three.jpeg'},
      {id: 6, name: 'Green tuxedo with pant',  mrp: 3000, discount: 90, image: 'assets/blazers/six-one.jpg',
      imgone: 'assets/blazers/six-two.jpg', imgtwo: 'assets/blazers/six-three.jpg'},
      {id: 7, name: 'Navy blue suite',  mrp: 2980, discount: 79, image: 'assets/blazers/seven-one.jpeg',
      imgone: 'assets/blazers/seven-two.jpeg', imgtwo: 'assets/blazers/seven-three.jpeg'},
      {id: 8, name: 'Red blazer 3 piece',  mrp: 8980, discount: 90, image: 'assets/blazers/eight-one.jpeg',
      imgone: 'assets/blazers/eight-two.jpeg', imgtwo: 'assets/blazers/eight-three.jpeg'},
      {id: 9, name: 'Royal blue Suite',  mrp: 4980, discount: 89, image: 'assets/blazers/nine-one.jpg',
      imgone: 'assets/blazers/nine-two.jpg', imgtwo: 'assets/blazers/nine-three.jpg'},
      {id: 10, name: 'Cream printed suite 2 piece',  mrp: 11980, discount: 94, image: 'assets/blazers/ten-one.jpeg',
      imgone: 'assets/blazers/ten-two.jpeg', imgtwo: 'assets/blazers/ten-three.jpeg'}
    ];

    const shirts = [
      {id: 11, name: 'Black Shirt',  mrp: 1980, discount: 90, image: 'assets/shirts/one-one.jpg',
      imgone: 'assets/shirts/one-two.jpg', imgtwo: 'assets/shirts/one-three.jpg'},
      {id: 12, name: 'Dark navy blue shirt',  mrp: 980, discount: 77, image: 'assets/shirts/three-one.jpg',
      imgone: 'assets/shirts/three-two.jpg', imgtwo: 'assets/shirts/three-three.jpg'},
      {id: 13, name: 'Blue shirt',  mrp: 2340, discount: 88, image: 'assets/shirts/two-one.jpg',
      imgone: 'assets/shirts/two-two.jpg', imgtwo: 'assets/shirts/two-three.jpg'},
      {id: 14, name: 'Light blue shirt',  mrp: 1720, discount: 90, image: 'assets/shirts/four-one.jpg',
      imgone: 'assets/shirts/four-two.jpg', imgtwo: 'assets/shirts/four-three.jpg'},
      {id: 15, name: 'Black shirt',  mrp: 1200, discount: 89, image: 'assets/shirts/five-one.jpg',
      imgone: 'assets/shirts/five-two.jpg', imgtwo: 'assets/shirts/five-three.jpg'},
      {id: 16, name: 'Off white shirt',  mrp: 2200, discount: 85, image: 'assets/shirts/six-one.jpg',
      imgone: 'assets/shirts/six-two.jpg', imgtwo: 'assets/shirts/six-three.jpg'},
      {id: 17, name: 'Light navy blue',  mrp: 2080, discount: 82, image: 'assets/shirts/seven-one.jpg',
      imgone: 'assets/shirts/seven-two.jpg', imgtwo: 'assets/shirts/seven-three.jpg'},
      {id: 18, name: 'Baby pink shirt',  mrp: 1990, discount: 86, image: 'assets/shirts/eight-one.jpg',
      imgone: 'assets/shirts/eight-two.jpg', imgtwo: 'assets/shirts/eight-three.jpg'},
      // {id: 19, name: 'Royal blue Suite',  mrp: 4980, discount: 89, image: 'assets/blazers/nine-one.jpg',
      // imgone: 'assets/blazers/nine-two.jpg', imgtwo: 'assets/blazers/nine-three.jpg'},
      // {id: 20, name: 'Cream printed suite 2 piece',  mrp: 11980, discount: 94, image: 'assets/blazers/ten-one.jpeg',
      // imgone: 'assets/blazers/ten-two.jpeg', imgtwo: 'assets/blazers/ten-three.jpeg'}
    ];

    const pants = [
      {id: 31, name: 'Black pant',  mrp: 2080, discount: 92, image: 'assets/pants/one-one.jpeg',
      imgone: 'assets/pants/one-two.jpeg', imgtwo: 'assets/pants/one-three.jpeg'},
      {id: 32, name: 'Black pant',  mrp: 980, discount: 80, image: 'assets/pants/three-one.jpeg',
      imgone: 'assets/pants/three-two.jpeg', imgtwo: 'assets/pants/three-three.jpeg'},
      {id: 33, name: 'Grey pant',  mrp: 1200, discount: 88, image: 'assets/pants/two-one.jpg',
      imgone: 'assets/pants/two-two.jpg', imgtwo: 'assets/pants/two-three.jpg'},
      {id: 34, name: 'Cream pant',  mrp: 1580, discount: 89, image: 'assets/pants/four-one.jpeg',
      imgone: 'assets/pants/four-two.jpeg', imgtwo: 'assets/pants/four-three.jpeg'},
      // {id: 35, name: 'Printed suite',  mrp: 1200, discount: 89, image: 'assets/shirts/five-one.jpg',
      // imgone: 'assets/shirts/five-two.jpg', imgtwo: 'assets/shirts/five-three.jpg'},
      // {id: 16, name: 'Green tuxedo with pant',  mrp: 3000, discount: 85, image: 'assets/shirts/six-one.jpg',
      // imgone: 'assets/shirts/six-two.jpg', imgtwo: 'assets/shirts/six-three.jpg'},
      // {id: 17, name: 'Navy blue suite',  mrp: 2980, discount: 82, image: 'assets/shirts/seven-one.jpg',
      // imgone: 'assets/shirts/seven-two.jpg', imgtwo: 'assets/shirts/seven-three.jpg'},
      // {id: 18, name: 'Red blazer 3 piece',  mrp: 2990, discount: 86, image: 'assets/shirts/eight-one.jpg',
      // imgone: 'assets/shirts/eight-two.jpg', imgtwo: 'assets/shirts/eight-three.jpg'},
      // {id: 19, name: 'Royal blue Suite',  mrp: 4980, discount: 89, image: 'assets/blazers/nine-one.jpg',
      // imgone: 'assets/blazers/nine-two.jpg', imgtwo: 'assets/blazers/nine-three.jpg'},
      // {id: 20, name: 'Cream printed suite 2 piece',  mrp: 11980, discount: 94, image: 'assets/blazers/ten-one.jpeg',
      // imgone: 'assets/blazers/ten-two.jpeg', imgtwo: 'assets/blazers/ten-three.jpeg'}
    ];

    return {blazers, shirts, pants };
  }
}
