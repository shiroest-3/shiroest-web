import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  faq: any = [];
  anotherfaq: any = [];
  quetion: string;
  inputString: string;
  showatfirst;
  el: any;
  constructor() { }

  ngOnInit() {
    this.faq = [
      {no: 1, quetion: 'How it works?', answer: 'Come down to the store, try the desire apparel you want, tell the number of days you want to rent accordingly, we will calculate and tell you the price along with refundable deposit.'},
      {no: 2, quetion: 'How many days I can keep the outfit with me?', answer: 'Let us know the number of days you want to rent, and we will tell you the exact day to return the outfit.'},
      {no: 3, quetion: 'Do I have to pay any security deposit?', answer: 'Yes, you have to give security deposit, which will be returned back once you return the product.'},
      {no: 4, quetion: 'After renting can I customize or alter my outfit?', answer: 'We don’t encourage you to do so. Any damage or alteration to the rent apparel will attract a penalty.'},
      {no: 5, quetion: 'What will happen if the dress is damaged by me?', answer: 'Our fabric expert will go through detail check of the product and accordingly the penalty will be applied.'},
      {no: 6, quetion: 'What will happen if I return the dress late?', answer: 'Since we strictly follow the timeline, returning dress late will attract a penalty.'},
      {no: 7, quetion: 'Do I need to dry clean the dress before or after using?', answer: 'We make sure that the dress hand over to you is hygiene and in good condition, so we dry clean before we hand over to you, and there is not need of dry clean again when you return it back.'},
      {no: 8, quetion: 'What is the time taken to process the refund deposit? ', answer: 'Normally it we refund the amount instantly, that is as soon as you return the product. But in the worst case it might take couple of days.'},
      {no: 9, quetion: 'Which an all cites are we available?', answer: 'We are a start up based out of Bangalore, so currently we are available only in Bangalore.'},
      {no: 10, quetion: 'Where is Shiroest store?', answer: 'We are located in 209 83rd cross Kumaraswamy Layout, near Kumaraswamy Layout Police station Bangalore – 560078'},
      {no: 11, quetion: 'Do you give door step delivery or pick up?', answer: 'At the moment we are not available for door step delivery or pick up. But we will be resuming this sometime soon.'},
      {no: 12, quetion: 'What method of payment do we accept?', answer: 'We accept all kind of payment method.'},
      {no: 13, quetion: 'I want to return the dress before the time period?', answer: 'No issue, you can contact us and let us know, depending on the time, we may refund some amount of money. '},
      {no: 14, quetion: 'How do I sell on Shiroest?', answer: 'Wooh!! Great choice. You can contact us and we will guide you with further procedure.'}
    ];

    this.showatfirst = true;
  }

  somesetting() {
   return this.anotherfaq = this.faq;
  }

  Search() {
    this.anotherfaq = this.somesetting();
    // console.log('works well--11', this.anotherfaq);
    this.anotherfaq = this.faq.filter(res=> {
      if (this.quetion !== '') {
        this.showatfirst = false;
        return res.quetion.toLocaleLowerCase().match(this.quetion.toLocaleLowerCase());
      } else {
        this.showatfirst = true;
      }
    });
  }
}
