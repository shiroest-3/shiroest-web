export class Blazers {
  id: number;
  image: string;
  name: string;
  mrp: number;
  discount: number;
  imgone: string;
  imgtwo: string;
    constructor( id: number,  image: string, name: string, mrp: number, discount: number, imgone: string, imgtwo: string) {

      this.id = id;
      this.image = image;
      this.name = name;
      this.mrp = mrp;
      this.discount = discount;
      this.imgone = imgone;
      this.imgtwo = imgtwo;
  }
}

export class Shirts {
  id: number;
  image: string;
  name: string;
  mrp: number;
  discount: number;
  imgone: string;
  imgtwo: string;
    constructor(id: number, image: string, name: string, mrp: number, discount: number, imgone: string, imgtwo: string) {
      this.id = id;
      this.image = image;
      this.name = name;
      this.mrp = mrp;
      this.discount = discount;
      this.imgone = imgone;
      this.imgtwo = imgtwo;
  }
}

export class Pants {
  id: number;
  image: string;
  name: string;
  mrp: number;
  discount: number;
  imgone: string;
  imgtwo: string;
    constructor(id: number, image: string, name: string, mrp: number, discount: number, imgone: string, imgtwo: string) {
      this.id = id;
      this.image = image;
      this.name = name;
      this.mrp = mrp;
      this.discount = discount;
      this.imgone = imgone;
      this.imgtwo = imgtwo;
  }
}

