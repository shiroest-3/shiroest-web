import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [

  ],

  imports: [
    CommonModule
  ],

  exports: [
    CommonModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatDialogModule
  ]
})
export class SharedModule { }
