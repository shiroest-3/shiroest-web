import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ContactUsDetailComponent } from './../dialogs/contact-us-detail/contact-us-detail.component';
import { OfferDetailComponent } from './../dialogs/offer-detail/offer-detail.component';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})

export class ContactUsComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  contactUsDetail() {
    this.dialog.open(ContactUsDetailComponent);
  }

  offersDetail() {
    this.dialog.open(OfferDetailComponent);
  }
}
