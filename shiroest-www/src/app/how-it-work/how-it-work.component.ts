import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-how-it-work',
  templateUrl: './how-it-work.component.html',
  styleUrls: ['./how-it-work.component.scss']
})
export class HowItWorkComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<HowItWorkComponent>
  ) { }

  ngOnInit() {
  }

 close() {
   this.dialogRef.close();
 }
}
