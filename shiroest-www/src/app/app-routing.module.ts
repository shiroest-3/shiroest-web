import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqComponent } from './faq/faq.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { BlazersComponent } from './product-list/blazers/blazers.component';
import { ProductDetailComponent } from './product-list/product-detail/product-detail.component';

import { PrivacyPolicyComponent } from './policies/privacy-policy/privacy-policy.component';
import { CancellationPolicyComponent } from './policies/cancellation-policy/cancellation-policy.component';
import { TermsConditionComponent } from './policies/terms-condition/terms-condition.component';
import { AboutUsComponent } from './information/about-us/about-us.component';
import { SupportUsComponent } from './information/support-us/support-us.component';
import { RentWithUsComponent } from './information/rent-with-us/rent-with-us.component';
import { ReferAndEarnComponent } from './information/refer-and-earn/refer-and-earn.component';
import { ShirtsComponent } from './product-list/shirts/shirts.component';
import { BowtieComponent } from './product-list/bowtie/bowtie.component';
import { PantsComponent } from './product-list/pants/pants.component';

import { AddressComponent } from './address/address.component';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { ContactUsDetailComponent } from './dialogs/contact-us-detail/contact-us-detail.component';
import { OfferDetailComponent } from './dialogs/offer-detail/offer-detail.component';

const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'faq', component: FaqComponent },
    {path: 'rent/blazer', component: BlazersComponent, pathMatch: 'full'},
    {path: 'rent/shirt', component: ShirtsComponent, pathMatch: 'full'},
    {path: 'rent/pant', component: PantsComponent, pathMatch: 'full'},
    {path: 'rent/rent-with-us', component: RentWithUsComponent},
    {path: 'rent/refer-and-earn', component: ReferAndEarnComponent},
    {path: 'rent/:url/:id', component: ProductDetailComponent},
    {path: 'privacy-policy', component: PrivacyPolicyComponent },
    {path: 'cancellation-policy', component: CancellationPolicyComponent},
    {path: 'terms-condition', component: TermsConditionComponent},
    {path: 'about-us', component: AboutUsComponent},
    {path: 'support-us', component: SupportUsComponent},
    {path: '**', component: BowtieComponent }
    // {path: 'bow-tie', component: BowtieComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const routingComponents = [BlazersComponent, FaqComponent, HomeComponent,
   ContactUsComponent, ProductDetailComponent, PrivacyPolicyComponent,
   CancellationPolicyComponent, TermsConditionComponent, AboutUsComponent, SupportUsComponent,
   RentWithUsComponent, ReferAndEarnComponent, ShirtsComponent, BowtieComponent, PantsComponent];

export const dialogsComponents = [ContactUsDetailComponent, OfferDetailComponent,
  AddressComponent, HowItWorkComponent];
