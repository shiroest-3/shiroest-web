import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rent-with-us',
  templateUrl: './rent-with-us.component.html',
  styleUrls: ['./rent-with-us.component.scss']
})
export class RentWithUsComponent implements OnInit {

  defaultImage = '/assets/logo/loader.gif';
  constructor() { }

  ngOnInit() {
  }

}
