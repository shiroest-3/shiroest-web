import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  we: any = [];
  defaultImage = '/assets/logo/loader.gif';
  constructor() { }

  ngOnInit() {
    this.we = [
      {no: 1, pic: '/assets/about-us/benai.png', name: 'Benai', position: 'Founder', description: 'He is the backbone behing our amazing website, when he is not into code you will find him in kitchen cooking some awesome and deleciouis food (spicy), and chill out with some good music.'},
      {no: 1, pic: '/assets/about-us/chetan.png', name: 'Chetan', position: 'Founder', description: 'He make sure that our website design look attractive and catchy also contribute to the website with good code, when free you will find him traveling and he is obssed about dance.'}
    ]
  }
}
