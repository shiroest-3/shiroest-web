import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  API_URL_BLAZERS: string = 'blazer/';
  API_URL_SHIRTS: string = 'shirt/';
  API_URL_PANTS: string = 'pant/';

  constructor(private httpClient: HttpClient) { }

  getProducts() {
    return this.httpClient.get(this.API_URL_BLAZERS + 'blazers');
  }

  getProduct(productId) {
    return this.httpClient.get(`${this.API_URL_BLAZERS + 'blazers'}/${productId}/ + 'blazer'`);
  }

  getShirts() {
    return this.httpClient.get(this.API_URL_SHIRTS + 'shirts');
  }

  getShirt(shirtId) {
    return this.httpClient.get(`${this.API_URL_SHIRTS + 'shirts'}/${shirtId}/ + 'shirt'`);
  }

  getPants() {
    return this.httpClient.get(this.API_URL_PANTS + 'pants');
  }

  getPant(pantId) {
    return this.httpClient.get(`${this.API_URL_PANTS + 'pants'}/${pantId}/ + 'pant`);
  }
}
